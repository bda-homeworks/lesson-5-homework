public class Main {
    public static void main(String[] args) {
        int[][] array = {
                {98, 20, 56},
                {25, 6, 43},
                {625, 19, 10}
        };
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j];
                System.out.print(array[i][j] + " ");
            }
        }
        System.out.print(" Bu reqemlerin toplamasi: " + sum);
    }
}