import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.print("Arrayin olcusu qeyd edin: ");
        Scanner scanner = new Scanner(System.in);
        int[] numbers = new int[scanner.nextInt()];
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(i + " indeksindeki reqemi daxil edin: ");
            numbers[i] = scanner.nextInt();
        }
        System.out.print("Arrayiniz: ");
        for (int number : numbers) {
            System.out.print(number + " ");
        }
        System.out.println("array artan sira ile duzulubmu: " + method(numbers));
    }
    public static boolean method(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > arr[i + 1] || arr[i+1] != 0) {
                return true;
            }
        }
        return false;
    }
}
