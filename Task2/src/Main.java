public class Main {
    public static void main(String[] args) {
        int[][] matrix = {{5, 2, 3},
                        {4, 2, 6},
                        {7, 8, 10}}; // sample matrix

        // left diagonal calculation
        int leftSum = 0;
        for (int i = 0; i < matrix.length; i++) {
            leftSum += matrix[i][i];
        }
        System.out.println("Left diagonal sum: " + leftSum);

        // right diagonal calculation
        int rightSum = 0;
        for (int i = 0; i < matrix.length; i++) {
            rightSum += matrix[i][matrix.length - i - 1];
        }
        System.out.println("Right diagonal sum: " + rightSum);
    }
}