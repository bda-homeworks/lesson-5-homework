import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the length of array:");
        int[] numbers = new int[input.nextInt()];
        int length = numbers.length;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println("Please enter the number:");
            numbers[i] = input.nextInt();
        }

        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum = sum + numbers[i];
        }
        double avarage = sum / length;

        System.out.println(avarage);
    }
}