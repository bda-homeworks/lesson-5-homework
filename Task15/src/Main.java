import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Choose the length of array: ");
        Scanner input = new Scanner(System.in);
        int[] numbers = new int[input.nextInt()];
        int length = numbers.length;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println("Reqemleri daxil edin:");
            numbers[i] = input.nextInt();
        }

        System.out.println("Axtarmaq istediyiniz reqemi qeyd edin:");
        int toFind = input.nextInt();

        System.out.println(toFind + " reqem  indexde: " + Arrays.binarySearch(numbers, toFind) + "-yerdedir");
    }
}