import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Axtarmaq istediyin reqemi qeyd ele:");
        int[] num = {2, 4, 5, 6, 7, 8, 11};
        Scanner input = new Scanner(System.in);
        int toFind = input.nextInt();
        boolean found = false;

        for (int n : num) {
            if (n == toFind) {
                found = true;
                break;
            }
        }

        if (found) {
            System.out.println(toFind + " Tapildi gozun aydin!");
            System.out.println(" Yerlesdiyi index: " + Arrays.binarySearch(num, toFind) );
        } else {
            System.out.println(toFind + " Axtar bala axtar! hele burdayiq");
        }
    }
}