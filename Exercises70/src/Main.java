import java.util.*;

public class Main {
    public static void main(String[] args) {
//        Scanner for some tasks
        Scanner input = new Scanner(System.in);
//        main1_10(input);
//        main11_20(input);


//        21. Write a Java program to convert an ArrayList to an array.
//        22. Write a Java program to find all pairs of elements in an array whose sum is equal to a specified number.
//        23. Write a Java program to test two arrays' equality.
//        24. Write a Java program to find a missing number in an array.
//        25. Write a Java program to find common elements in three sorted (in non-decreasing order) arrays.
//        26. Write a Java program to move all 0's to the end of an array. Maintain the relative order of the other (non-zero) array elements.
//        27. Write a Java program to find the number of even and odd integers in a given array of integers.
//        28. Write a Java program to get the difference between the largest and smallest values in an array of integers. The array must have a length of at least 1.
//        29. Write a Java program to compute the average value of an array of integers except the largest and smallest values.
//        30. Write a Java program to check if an array of integers is without 0 and -1.
//        31. Write a Java program to check if the sum of all the 10's in the array is exactly 30. Return false if the condition does not satisfy, otherwise true.
//        32. Write a Java program to check if an array of integers contains two specified elements 65 and 77.
//        33. Write a Java program to remove duplicate elements from a given array and return the updated array length.
//                Sample array: [20, 20, 30, 40, 50, 50, 50]
//        After removing the duplicate elements the program should return 4 as the new length of the array.
//        34. Write a Java program to find the length of the longest consecutive elements sequence from an unsorted array of integers.
//        Sample array: [49, 1, 3, 200, 2, 4, 70, 5]
//        The longest consecutive elements sequence is [1, 2, 3, 4, 5], therefore the program will return its length 5.
//        35. Write a Java program to find the sum of the two elements of a given array equal to a given integer.
//                Sample array: [1,2,4,5,6]
//        Target value: 6.
//        36. Write a Java program to find all the distinct triplets such that the sum of all the three elements [x, y, z (x ≤ y ≤ z)] equal to a specified number.
//                Sample array: [1, -2, 0, 5, -1, -4]
//        Target value: 2.
//        37. Write a Java program to create an array of its anti-diagonals from a given square matrix.
//
//                Example:
//        Input :
//        1 2
//        3 4
//        Output:
//[
//[1],
//[2, 3],
//[4]
//]
//        38. Write a Java program to get the majority element from an array of integers containing duplicates.
//
//        Majority element: A majority element is an element that appears more than n/2 times where n is the array size.
//        39. Write a Java program to print all the LEADERS in the array.
//        Note: An element is leader if it is greater than all the elements to its right side.
//        40. Write a Java program to find the two elements in a given array of positive and negative numbers such that their sum is close to zero.
//        41. Write a Java program to find the smallest and second smallest elements of a given array.
//        42. Write a Java program to separate 0s and 1s in an array of 0s and 1s into left and right sides.
//        43. Write a Java program to find all combinations of four elements of an array whose sum is equal to a given value.
//        44. Write a Java program to count the number of possible triangles from a given unsorted array of positive integers.
//                Note: The triangle inequality states that the sum of the lengths of any two sides of a triangle must be greater than or equal to the length of the third side.
//        45. Write a Java program to cyclically rotate a given array clockwise by one.
//        46. Write a Java program to check whether there is a pair with a specified sum in a given sorted and rotated array.
//        47. Write a Java program to find the rotation count in a given rotated sorted array of integers.
//        48. Write a Java program to arrange the elements of an array of integers so that all negative integers appear before all positive integers.
//        49. Write a Java program to arrange the elements of an array of integers so that all positive integers appear before all negative integers.
//        50. Write a Java program to sort an array of positive integers from an array. In the sorted array the value of the first element should be maximum, the second value should be a minimum, third should be the second maximum, the fourth should be the second minimum and so on.
//        51. Write a Java program that separates 0s on the left hand side and 1s on the right hand side from a random array of 0s and 1.
//        52. Write a Java program to separate even and odd numbers from a given array of integers. Put all even numbers first, and then odd numbers.
//        53. Write a Java program to replace every element with the next greatest element (from the right side) in a given array of integers.
//        There is no element next to the last element, therefore replace it with -1.
//        54. Write a Java program to check if a given array contains a subarray with 0 sum.
//
//                Example:
//        Input :
//        nums1= { 1, 2, -2, 3, 4, 5, 6 }
//        nums2 = { 1, 2, 3, 4, 5, 6 }
//        nums3 = { 1, 2, -3, 4, 5, 6 }
//        Output:
//        Does the said array contain a subarray with 0 sum: true
//        Does the said array contain a subarray with 0 sum: false
//        Does the said array contain a subarray with 0 sum: true
//        55. Write a Java program to print all sub-arrays with 0 sum present in a given array of integers.
//
//        Example:
//        Input :
//        nums1 = { 1, 3, -7, 3, 2, 3, 1, -3, -2, -2 }
//        nums2 = { 1, 2, -3, 4, 5, 6 }
//        nums3= { 1, 2, -2, 3, 4, 5, 6 }
//        Output:
//        Sub-arrays with 0 sum : [1, 3, -7, 3]
//        Sub-arrays with 0 sum : [3, -7, 3, 2, 3, 1, -3, -2]
//
//        Sub-arrays with 0 sum : [1, 2, -3]
//        Sub-arrays with 0 sum : [2, -2]
//        56. Write a Java program to sort a binary array in linear time.
//        From Wikipedia,
//        Linear time: An algorithm is said to take linear time, or O(n) time, if its time complexity is O(n). Informally, this means that the running time increases at most linearly with the size of the input. More precisely, this means that there is a constant c such that the running time is at most cn for every input of size n. For example, a procedure that adds up all elements of a list requires time proportional to the length of the list, if the adding time is constant, or, at least, bounded by a constant.
//                Linear time is the best possible time complexity in situations where the algorithm has to sequentially read its entire input. Therefore, much research has been invested into discovering algorithms exhibiting linear time or, at least, nearly linear time. This research includes both software and hardware methods. There are several hardware technologies which exploit parallelism to provide this. An example is content-addressable memory. This concept of linear time is used in string matching algorithms such as the Boyer–Moore algorithm and Ukkonen's algorithm.
//
//        Example:
//        Input :
//        b_nums[] = { 0, 1, 1, 0, 1, 1, 0, 1, 0, 0 }
//        Output:
//        After sorting: [0, 0, 0, 0, 0, 1, 1, 1, 1, 1]
//        57. Write a Java program to check if a sub-array is formed by consecutive integers from a given array of integers.
//
//                Example:
//        Input :
//        nums = { 2, 5, 0, 2, 1, 4, 3, 6, 1, 0 }
//        Output:
//        The largest sub-array is [1, 7]
//        Elements of the sub-array: 5 0 2 1 4 3 6
//        58. Given two sorted arrays A and B of size p and q, write a Java program to merge elements of A with B by maintaining the sorted order i.e. fill A with first p smallest elements and fill B with remaining elements.
//
//                Example:
//        Input :
//        int[] A = { 1, 5, 6, 7, 8, 10 }
//        int[] B = { 2, 4, 9 }
//        Output:
//        Sorted Arrays:
//        A: [1, 2, 4, 5, 6, 7]
//        B: [8, 9, 10]
//        59. Write a Java program to find the maximum product of two integers in a given array of integers.
//
//        Example:
//        Input :
//        nums = { 2, 3, 5, 7, -7, 5, 8, -5 }
//        Output:
//        Pair is (7, 8), Maximum Product: 56
//        60. Write a Java program to shuffle a given array of integers.
//
//                Example:
//        Input :
//        nums = { 1, 2, 3, 4, 5, 6 }
//        Output:
//        Shuffle Array: [4, 2, 6, 5, 1, 3]
//        61. Write a Java program to rearrange a given array of unique elements such that every second element of the array is greater than its left and right elements.
//
//        Example:
//        Input :
//        nums= { 1, 2, 4, 9, 5, 3, 8, 7, 10, 12, 14 }
//        Output:
//        Array with every second element is greater than its left and right elements:
//[1, 4, 2, 9, 3, 8, 5, 10, 7, 14, 12]
//        62. Write a Java program to find equilibrium indices in a given array of integers.
//
//        Example:
//        Input :
//        nums = {-7, 1, 5, 2, -4, 3, 0}
//        Output:
//        Equilibrium indices found at : 3
//        Equilibrium indices found at : 6
//        63. Write a Java program to replace each element of the array with the product of every other element in a given array of integers.
//
//        Example:
//        Input :
//        nums1 = { 1, 2, 3, 4, 5, 6, 7}
//        nums2 = {0, 1, 2, 3, 4, 5, 6, 7}
//        Output:
//        Array with product of every other element:
//[5040, 2520, 1680, 1260, 1008, 840, 720]
//        Array with product of every other element:
//[5040, 0, 0, 0, 0, 0, 0, 0]
//        64. Write a Java program to find the Longest Bitonic Subarray in a given array.
//
//        A bitonic subarray is a subarray of a given array where elements are first sorted in increasing order, then in decreasing order. A strictly increasing or strictly decreasing subarray is also accepted as bitonic subarray.
//
//                Example:
//        Input :
//        nums = { 4, 5, 9, 5, 6, 10, 11, 9, 6, 4, 5 }
//        Output:
//        The longest bitonic subarray is [3,9]
//        Elements of the said sub-array: 5 6 10 11 9 6 4
//        The length of longest bitonic subarray is 7
//        65. Write a Java program to find the maximum difference between two elements in a given array of integers such that the smaller element appears before the larger element.
//
//        Example:
//        Input :
//        nums = { 2, 3, 1, 7, 9, 5, 11, 3, 5 }
//        Output:
//        The maximum difference between two elements of the said array elements
//        10
//        66. Write a Java program to find a contiguous subarray within a given array of integers with the largest sum.
//
//                In computer science, the maximum sum subarray problem is the task of finding a contiguous subarray with the largest sum, within a given one-dimensional array A[1...n] of numbers. Formally, the task is to find indices and with, such that the sum is as large as possible.
//
//        Example:
//        Input :
//        int[] A = {1, 2, -3, -4, 0, 6, 7, 8, 9}
//        Output:
//        The largest sum of contiguous sub-array: 30
//        67. Write a Java program to find the subarray with the largest sum in a given circular array of integers.
//
//                Example:
//        Input :
//        nums1 = { 2, 1, -5, 4, -3, 1, -3, 4, -1 }
//        nums2 = { 1, -2, 3, 0, 7, 8, 1, 2, -3 }
//        Output:
//        The sum of subarray with the largest sum is 6
//        The sum of subarray with the largest sum is 21
//        68. Write a Java program to create all possible permutations of a given array of distinct integers.
//
//        Example:
//        Input :
//        nums1 = {1, 2, 3, 4}
//        nums2 = {1, 2, 3}
//        Output:
//        Possible permutations of the said array:
//[1, 2, 3, 4]
//[1, 2, 4, 3]
//....
//[4, 1, 3, 2]
//[4, 1, 2, 3]
//        Possible permutations of the said array:
//[1, 2, 3]
//[1, 3, 2]
//...
//[3, 2, 1]
//[3, 1, 2]
//        69. Write a Java program to find the minimum subarray sum of specified size in a given array of integers.
//
//                Example:
//        Input :
//        nums = { 1, 2, 3, 4, 5, 6, 7, 8, 9,10}
//        Output:
//        Sub-array size: 4
//        Sub-array from 0 to 3 and sum is: 10
//        70. Write a Java program to find the smallest length of a contiguous subarray of which the sum is greater than or equal to a specified value. Return 0 instead.
//
//                Example:
//        Input :
//        nums = {1, 2, 3, 4, 6}
//        Output:
//        Minimum length of a contiguous subarray of which the sum is 8, 2
//        71. Write a Java program to find the largest number from a given list of non-negative integers.
//
//                Example:
//        Input :
//        nums = {1, 2, 3, 0, 4, 6}
//        Output:
//        Largest number using the said array numbers: 643210
//        72. Write a Java program to find and print one continuous subarray (from a given array of integers) that if you only sort the said subarray in ascending order then the entire array will be sorted in ascending order.
//
//                Example:
//        Input :
//        nums1 = {1, 2, 3, 0, 4, 6}
//        nums2 = { 1, 3, 2, 7, 5, 6, 4, 8}
//        Output:
//        Continuous subarray:
//        1 2 3 0
//        Continuous subarray:
//        3 2 7 5 6 4
//        73. Write a Java program to sort a given array of distinct integers where all its numbers are sorted except two numbers.
//
//                Example:
//        Input :
//        nums1 = { 3, 5, 6, 9, 8, 7 }
//        nums2 = { 5, 0, 1, 2, 3, 4, -2 }
//        Output:
//        After sorting new array becomes: [3, 5, 6, 7, 8, 9]
//        After sorting new array becomes: [-2, 0, 1, 2, 3, 4, 5]
//        74. Write a Java program to find all triplets equal to a given sum in an unsorted array of integers.
//
//                Example:
//        Input :
//        nums = { 1, 6, 3, 0, 8, 4, 1, 7 }
//        Output:
//        Triplets of sum 7
//        (0 1 6)
//        (0 3 4)
//        75. Write a Java program to calculate the largest gap between sorted elements of an array of integers.
//
//                Example:
//        Original array: [23, -2, 45, 38, 12, 4, 6]
//        Largest gap between sorted elements of the said array: 15
//        76. Write a Java program to determine whether numbers in an array can be rearranged so that each number appears exactly once in a consecutive list of numbers. Return true otherwise false.
//
//                Example:
//        Original array: [1, 2, 5, 0, 4, 3, 6]
//        Check consecutive numbers in the said array!true
//        77. Write a Java program that checks whether an array of integers alternates between positive and negative values.
//
//                Example:
//        Original array: [1, -2, 5, -4, 3, -6]
//        Check the said array of integers alternates between positive and negative values!true
//        78. Write a Java program that checks whether an array is negative dominant or not. If the array is negative dominant return true otherwise false.
//
//                Example:
//        Original array of numbers:
//[1, -2, -5, -4, 3, -6]
//        Check Negative Dominance in the said array!true
//        79. Write a Java program that returns the missing letter from an array of increasing letters (upper or lower). Assume there will always be one omission from the array.
//
//                Example:
//        Original array of elements:
//[p, r, s, t]
//        Missing letter in the said array: q
    }
    public static void main1_10 (Scanner input) {
        //        1. Write a Java program to sort a numeric array and a string array.
        task1();
        //        2. Write a Java program to sum values of an array.
        task2();
        //        3. Write a Java program to print the following grid.
        task3();
        //        4. Write a Java program to calculate the average value of array elements.
        task4();
        //        5. Write a Java program to test if an array contains a specific value.
        task5(input);
        //        6. Write a Java program to find the index of an array element.
        task6(input);
        //        7. Write a Java program to remove a specific element from an array.
        task7(input);
        //        8. Write a Java program to copy an array by iterating the array.
        task8();
        //        9. Write a Java program to insert an element (specific position) into an array.
        task9(input);
        //        10. Write a Java program to find the maximum and minimum value of an array.
        task10();
    }
    public static void main11_20 (Scanner input) {
        //        11. Write a Java program to reverse an array of integer values.
        task11();
        //        12. Write a Java program to find duplicate values in an array of integer values.
        task12();
        //        13. Write a Java program to find duplicate values in an array of string values.
        task13();
        //        14. Write a Java program to find common elements between two arrays (string values).
        task14();
        //        15. Write a Java program to find common elements between two integer arrays.
        task15();
        //        16. Write a Java program to remove duplicate elements from an array.
        task16();
        //        17. Write a Java program to find the second largest element in an array.

        //        18. Write a Java program to find the second smallest element in an array.

        //        19. Write a Java program to add two matrices of the same size.

        //        20. Write a Java program to convert an array to an ArrayList.

    }
    public static void task1() {
        System.out.println("Task - 1");
        int[] num={4,2,7,9,8};
        String[] str={"Qarpiz", "Banan", "Armud", "Alma"};

        Arrays.sort(num);
        Arrays.sort(str);

        System.out.println(Arrays.toString(num));
        System.out.println(Arrays.toString(str));
        System.out.println("----------------------------------------");
    }
    public static void task2 () {
        System.out.println("Task - 2");
        int[] num={1,2,3,4,5};
        int sum = 0;
        for (int numbers : num) {
            sum = numbers + sum;
        }
        System.out.println(sum);
        System.out.println("----------------------------------------");
    }
    public static void task3 () {
        System.out.println("Task - 3");
        for (int i = 0; i < 10; i++) {
            for (int k = 0; k < 10; k++) {
                System.out.print("-");
            }
            System.out.println();
        }

        int [][]a = new int[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.printf("%2d ", a[i][j]);
            }
            System.out.println();
        }
        System.out.println("----------------------------------------");
    }

    public static void task4() {
        System.out.println("Task - 4");
        int[] num={1,2,3,4,5};
        int sum = 0;
        int avg = 0;
        for (int numbers : num) {
            sum = numbers + sum;
        }
        avg = sum / num.length;
        System.out.println("avg of the array is: " + avg);
        System.out.println("----------------------------------------");
    }

    public static void task5 (Scanner input) {
        System.out.println("Task - 5");
        int[] num={1,2,3,4,5,6,7,8,9};
        for (int numbers : num) {
            System.out.print(numbers + ",");
        }
        System.out.println();
        System.out.print("Enter the value to check if it contains in array: ");
        int number = input.nextInt();
        int foundnumber = 0;
        for (int numbers : num) {
            if (number == numbers) {
//                System.out.println(numbers + " this contains inside array.");
                foundnumber = number;
            } else {
//                System.out.println("no such number like - " + number + " inside array" );
            }
        }
        if (foundnumber == number) {
            System.out.println(foundnumber + " this contains inside array.");
        } else {
            System.out.println("no such number like - " + number + " inside array");
        }
        System.out.println("----------------------------------------");
    }

    public static void task6 (Scanner input) {
        System.out.println("Task - 6");
        System.out.print("Our array: ");
        int[] num={1,2,3,4,5,6,7,8,9};
        for (int numbers : num) {
            System.out.print(numbers + ",");
        }
        System.out.println();
        System.out.print("Enter the number to show it's index: ");
        int index = input.nextInt();
//        int indexNumber = Arrays.asList(num).indexOf(index);
//        System.out.println(indexNumber + " - number on the index");
        int foundIndex = -1;
        for (int i = 0; i < num.length; i++) {
            if (index == num[i]) {
                foundIndex = i;
            }
        }


        if (foundIndex != -1) {
            System.out.println("Value " + index + " found at index " + foundIndex);
        } else {
            System.out.println("Value " + index + " not found in array");
        }
        System.out.println("----------------------------------------");
    }

    public static void task7 (Scanner input) {
        System.out.println("Task - 7");

        int[] arr = {5, 10, 15, 20, 25};
        System.out.println("Array: " + Arrays.toString(arr));
        System.out.print("Enter the number to remove: ");
        int elementToRemove = input.nextInt();

        int indexToRemove = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == elementToRemove) {
                indexToRemove = i;
                break;
            }
        }

        if (indexToRemove != -1) {
            for (int i = indexToRemove; i < arr.length - 1; i++) {
                arr[i] = arr[i + 1];
            }

            arr = Arrays.copyOf(arr, arr.length - 1);

            System.out.println("Array after removing " + elementToRemove + ": " + Arrays.toString(arr));
        } else {
            System.out.println("Element " + elementToRemove + " not found in the array.");
        }
        System.out.println("----------------------------------------");
    }

    public static void task8 () {
        System.out.println("Task - 8");
        int[] my_array = {25, 14, 56, 15, 36, 56, 77, 18, 29, 49};
        int[] new_array = new int[10];

        for (int i = 0; i < new_array.length; i++) {
            new_array[i] = my_array[i];
        }

        System.out.println(Arrays.toString(new_array) + " Copy");

        System.out.println(Arrays.toString(my_array) + " Original");

        System.out.println("----------------------------------------");
    }

    public static void task9 (Scanner input) {
        System.out.println("Task - 9");
        int[] my_array = {25, 14, 56, 15, 36, 56, 77, 18, 29, 49};
        int[] new_array = new int[my_array.length + 1];

        System.out.println(Arrays.toString(my_array));

        System.out.print("Enter the index to add number: ");
        int index = input.nextInt();
        System.out.print("Enter the number: ");
        int number = input.nextInt();

        for (int i = 0, j = 0; i < new_array.length; i++) {
            if (i == index) {
                new_array[i] = number;
            } else {
                new_array[i] = my_array[j];
                j++;
            }
        }

        System.out.println(Arrays.toString(new_array));
        System.out.println("----------------------------------------");

    }

    public static void task10 () {
        System.out.println("Task - 10");
        int[] my_array = {25, 14, 56, 15, 6, 36, 56, 77, 18, 29, 49, 11};
        System.out.println(Arrays.toString(my_array));

        int maxValue = my_array[0];
        int minValue = my_array[0];

        for (int i = 0; i < my_array.length; i++) {
            if (my_array[i] > maxValue) {
                maxValue = my_array[i];
            }
        }

        for (int i = 0; i < my_array.length; i++) {
            if (my_array[i] < minValue) {
                minValue = my_array[i];
            }
        }

        System.out.println(maxValue + " Max value of array");
        System.out.println(minValue + " Min value of array");

        System.out.println("----------------------------------------");
    }

    public static void task11 () {
        System.out.println("Task - 11");
        int[] my_array1 = {1789, 2035, 1899, 1456, 2013, 1458, 2458, 1254, 1472, 2365, 1456, 2165, 1457, 2456};

        System.out.print("Original array: ");
        for ( int number : my_array1) {
            System.out.print(number + ", ");
        }
        System.out.println();

        System.out.print("Reversed array: ");
        for (int i = 0; i < my_array1.length/2; i++) {
            int tempNumber = my_array1[i];
            my_array1[i] = my_array1[my_array1.length - i - 1];
            my_array1[my_array1.length - i - 1] = tempNumber;
        }
        System.out.println(Arrays.toString(my_array1));

        System.out.println("----------------------------------------");
    }

    public static void task12 () {
        System.out.println("Task - 12");

        int[] my_array = {1, 2, 5, 5, 6, 6, 7, 2, 5};

        for (int i = 0; i < my_array.length; i++) {
            int count = 0;
            int number = my_array[i];
            boolean isDistinct = true;

            for (int j = 0; j < i; j++) {
                if (number == my_array[j]) {
                    isDistinct = false;
                    break;
                }
            }

            if (isDistinct) {
                for (int j = 0; j < my_array.length; j++) {
                    if (number == my_array[j]) {
                        count++;
                    }
                }
                System.out.println(number + " number appeared " + count + " times");
            }
        }
        System.out.println("----------------------------------------");
    }

    public static void task13 () {
        System.out.println("Task - 13");

        String[] my_array = {"bcd", "abd", "jude", "bcd", "oiu", "gzw", "oiu"};
        for (int i = 0; i < my_array.length; i++) {
            int count = 0;
            String word = my_array[i];
            for (int j = 0; j < my_array.length; j++) {
                if (word == my_array[j]) {
                    count++;
                }
            }
            if (count > 1) {
                System.out.println("Duplicate is : " + word);
            };
        }

        System.out.println("Second solution");

        for (int i = 0; i < my_array.length; i++) {
            for (int j = i + 1;j < my_array.length; j++) {
                if (my_array[i] == my_array[j] && (i != j)) {
                    System.out.println("Duplicate element: " + my_array[j]);
                }
            }
        }

        System.out.println("----------------------------------------");
    }

    public static void task14 () {
        System.out.println("Task - 14");

        String[] array1 = {"Python", "JAVA", "PHP", "C#", "C++", "SQL"};
        String[] array2 = {"MySQL", "SQL", "SQLite", "Oracle", "PostgreSQL", "DB2", "JAVA"};

        System.out.println("Array1 : "+Arrays.toString(array1));
        System.out.println("Array2 : "+Arrays.toString(array2));

        for (int i = 0; i < array1.length; i++) {
            String word1 = array1[i];
            for (int j = 0; j < array2.length; j++) {
                if (word1 == array2[j]) {
                    String word2 = array2[j];
                    System.out.println("Common element is: " + word2);
                }
            }
        }

        System.out.println("Other solution:");

        HashSet<String> set = new HashSet<String>();

        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i].equals(array2[j])) {
                    set.add(array2[j]);
                }
            }
        }

        System.out.println("Common elements are: " + set);

        System.out.println("----------------------------------------");
    }

    public static void task15 () {
        System.out.println("Task - 15");

        int[] array1 = {1, 2, 5, 5, 8, 9, 7, 10, 1};
        int[] array2 = {1, 0, 6, 15, 6, 4, 7, 0, 7};

        System.out.println("Array1 : " + Arrays.toString(array1));
        System.out.println("Array2 : " + Arrays.toString(array2));

        for (int i = 0; i < array1.length; i++) {
            for (int j = i + 1; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    System.out.println("Common numbers in arrays are: " + array2[j]);
                }
            }
        }
        System.out.println("----------------------------------------");
    }

    public static void task16 () {

    }
}

