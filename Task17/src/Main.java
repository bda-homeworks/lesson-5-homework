public class Main {
    public static void main(String[] args) {
        int [] arr = {1, 2, 3, 5, 6, 8, 9};
        System.out.print("Adi halda array: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

        System.out.print("Array bas ayaq versiyasi: ");
        for (int i = arr.length-1; i >= 0; i--) {
            System.out.print(arr[i] + " ");
        }
    }
}