import com.sun.jdi.ArrayReference;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String[] names = {"Abbas", "Leman", "Xedice", "Ilyas", "Nurlan", "Nihat", "Elchin", "Murad", "Mirhesen", "Emin", "Farid", "Terane"};
        Scanner input = new Scanner(System.in);
        System.out.println("Adi daxil edin: ");
        String searchName = input.nextLine().toLowerCase();

        int index = -1;
        for (int i = 0; i < names.length; i++) {
            if (names[i].toLowerCase().contains(searchName)) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            System.out.println("Tapildigi yer: " + index);
        } else {
            System.out.println("Ad tapilmadi :( Xosqedeme muraciet edin");
        }

        System.out.println("Hemen ad:");
        for (String name : names) {
            if (name.toLowerCase().contains(searchName)) { // Convert name to lowercase before checking
                System.out.println(name);
            }
        }
    }
}